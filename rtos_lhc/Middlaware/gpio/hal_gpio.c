/**
 * @file    hal_gpio.c
 * @author  Jorge Guzman (jorge.gzm@gmail.com); Rafael lopes (faellf@hotmail.com);
 * @date    Jun 26, 2015
 * @version 0.1.0.0 (beta)
 * @brief   Codigo da lib driver GPIO para o uso e acesso aos pinos de I/O do TM4C123GH6PM
 * @details
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 */

//==============================================================================
// INCLUDE FILES
//==============================================================================

#include "hal_gpio.h"
#include "bitwise/bitwise.h"
#include "MKL25Z4.h"

//==============================================================================
// PRIVATE DEFINITIONS
//==============================================================================


//==============================================================================
// PRIVATE TYPEDEFS
//==============================================================================

//==============================================================================
// PRIVATE VARIABLES			
//==============================================================================

//==============================================================================
// PRIVATE FUNCTIONS
//==============================================================================

//==============================================================================
// SOURCE CODE
//==============================================================================

int digitalWrite(int pin, int value)
{
	switch (pin)
	{
	case PA4:  wr_bit(GPIOA->PDOR,  4, value); return 0;
	case PA5:  wr_bit(GPIOA->PDOR,  5, value); return 0;
	case PA13: wr_bit(GPIOA->PDOR, 13, value); return 0;

	case PC8:  wr_bit(GPIOC->PDOR,  8, value); return 0;
	case PC9:  wr_bit(GPIOC->PDOR,  9, value); return 0;

	case PB18: wr_bit(GPIOB->PDOR, 18, value); return 0;
	case PB19: wr_bit(GPIOB->PDOR, 19, value); return 0;

	case PD0:  wr_bit(GPIOD->PDOR,  0, value); return 0;
	case PD1:  wr_bit(GPIOD->PDOR,  1, value); return 0;
	case PD5:  wr_bit(GPIOD->PDOR,  5, value); return 0;

	default: return -1;
	}
}

int digitalRead(int pin)
{
	switch (pin)
	{
	case PA4:  tst_bit(GPIOA->PDOR,  4); return 0;
	case PA5:  tst_bit(GPIOA->PDOR,  5); return 0;
	case PA13: tst_bit(GPIOA->PDOR, 13); return 0;

	case PC8:  tst_bit(GPIOC->PDOR,  8); return 0;
	case PC9:  tst_bit(GPIOC->PDOR,  9); return 0;

	case PB18: tst_bit(GPIOB->PDOR, 18); return 0;
	case PB19: tst_bit(GPIOB->PDOR, 19); return 0;

	case PD0:  tst_bit(GPIOD->PDOR,  0); return 0;
	case PD1:  tst_bit(GPIOD->PDOR,  1); return 0;
	case PD5:  tst_bit(GPIOD->PDOR,  5); return 0;

	default: return -1;
	}
}

//void pinMode(int pin, int value)
//{
//	switch (pin)
//	{
//
////		case PA2:  wr_bit(GPIOA->DIR, 2,  value);  break;
//	}
//}
