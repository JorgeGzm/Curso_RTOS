/**
  * @file    hal_gpio.h
  * @author  Jorge Guzman (jorge.gzm@gmail.com); Rafael lopes (faellf@hotmail.com); 
  * @date    Jun 26, 2015
  * @version 0.1.0.0 (beta)
  * @brief   Bibliteoca para o uso e acesso aos pinos de I/O do TM4C123GH6PM.
  * @details
  * @section LICENSE
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License as
  * published by the Free Software Foundation; either version 2 of
  * the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * General Public License for more details at
  * http://www.gnu.org/copyleft/gpl.html
*/

#ifndef HAL_GPIO_H
	#define	HAL_GPIO_H

//==============================================================================
// INCLUDE FILES
//==============================================================================

#include <stdint.h>

//==============================================================================
// PUBLIC DEFINITIONS
//==============================================================================

#define OUTPUT 0
#define INPUT  1

#define LOW  0
#define HIGH 1

#define PA4  0
#define PA5  1
#define PA13 2
#define PC8  3
#define PC9  4
#define PB18 5
#define PB19 6
#define PD0  7
#define PD1  8
#define PD5  9


//==============================================================================
// PUBLIC TYPEDEFS
//==============================================================================

//==============================================================================
// PUBLIC VARIABLES			
//==============================================================================

//==============================================================================
// PUBLIC FUNCTIONS
//==============================================================================

/**
 * Le o valor de um pino digital especificado
 * @param pin o numero do pin digital que voce quer ler
 * @return HIGH ou LOW
 */
int digitalRead(int pin);

/**
 *
 */
int digitalReadPortB(int pin);

/**
 * Escreve um valor HIGH ou um LOW em um pino digital
 * @param pin O numero do pin
 * @param value HIGH ou LOW
 */
int digitalWrite(int pin, int value);

/**
 * configura um pino como entrada ou saida digital
 * @param pin O numero do pin
 * @param type
 * @warning XXX funcao incompleta
 * @
 */
void pinMode(int pin, int type);

/**
 *
 * @param pin
 * @param func
 */
void gpio_attachISR_callback(uint8_t pin, void(*func)(void));

#endif	
