/**
  * @file    xprintf.c
  * @author  Jorge Guzman (jorge.gzm@gmail.com);
  * @date    Set 26, 2015
  * @version 0.2.0.0 (beta)
  * @brief   TODO documentar
  * @details
  * @section LICENSE
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License as
  * published by the Free Software Foundation; either version 2 of
  * the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * General Public License for more details at
  * http://www.gnu.org/copyleft/gpl.html
*/

//==============================================================================
// INCLUDE FILES
//==============================================================================

#include "xprintf.h"
#include <stdbool.h>

//==============================================================================
// PRIVATE DEFINITIONS
//==============================================================================

//==============================================================================
// PRIVATE TYPEDEFS
//==============================================================================

//==============================================================================
// PRIVATE VARIABLES			
//==============================================================================

//==============================================================================
// PRIVATE FUNCTIONS
//==============================================================================

/**
 * @brief Envia uma string pela UART
 * @param index TODO documentar
 * @param fmt pinteiro para a string que sera enviada.
 */
static void xprint_string(void (*func)(uint8_t), const uint8_t *fmt);

/**
 * @brief Envia uma variavel unsigned int pela UART
 * @param index TODO documentar
 * @param value valor que sera enviado
 */
static void xprint_UINT16(void (*func)(uint8_t), uint16_t value);

/**
 * @brief Envia uma variavel int pela UART
 * @param index TODO documentar
 * @param value TODO documentar
 */
static void xprint_INT16(void (*func)(uint8_t), int16_t value);

/**
 */
static void xprint_putc(void (*func)(uint8_t), uint8_t c);

/**
 */
static void xprint_float(void (*func)(uint8_t), float value);

//==============================================================================
// SOURCE CODE
//==============================================================================

static void xprint_putc(void (*func)(uint8_t), uint8_t c)
{
  	func(c);
}

static void xprint_string(void (*func)(uint8_t), const uint8_t *fmt)
{
    uint8_t c;

    while((c = *fmt) != '\0')
    {
        xprint_putc(func, c);
        fmt++;
    }
}

static void xprint_UINT16(void (*func)(uint8_t), uint16_t value)
{
    unsigned cnt = 0;
    uint8_t buffer[11];

    do
    {
        buffer[cnt] = value % 10; // Resto da divisao, sempre o primeiro digito
        value = value / 10; //exclui o digito fazendo a divisao
        cnt++; //incrementa contador
    }
    while(value); // repete ate valor n==0

    for(; cnt; cnt--)
    {
        xprint_putc(func, buffer[cnt - 1] + '0'); //soma o valor do buffer com o valor do caracter zero 0x30.
    }
}

static void xprint_INT16(void (*func)(uint8_t), int16_t value)
{
	unsigned cnt = 0;
    uint8_t flag_negativo;
    volatile uint8_t buffer[11];

    flag_negativo = 0;

    if(value < 0)
    {
        value = -value;
        flag_negativo = 1;
    }
    
    do
    {
        buffer[cnt] = value % 10; // Resto da divisao, sempre o primeiro digito
        value = value / 10; //exclui o digito fazendo a divisao
        cnt++; //incrementa contador
    }
    while(value); // repete ate valor n==0

    if(flag_negativo)
    {
        //cnt++;
        buffer[++cnt] = 0x2D;//'-'
    }

    for(; cnt; cnt--)
    {
        if(buffer[cnt] == 0x2D)
        {
            xprint_putc(func, 0x2D);
        }
        else
        {
            xprint_putc(func, buffer[cnt - 1] + '0'); //soma o valor do buffer com o valor do caracter zero 0x30.
        }
    }
}

static void xprint_float(void (*func)(uint8_t), float value)
{
		float afterPoint2 = 0;
		uint16_t afterPoint = 0;
		uint16_t beforePoint = 0;
		unsigned cnt = 0;
	    uint8_t flag_negativo = 0;
	    volatile uint8_t buffer[24];

	    if(value < 0)
		{
			value *= -1;
			flag_negativo = 1;
		}

	    beforePoint = (uint16_t)(value);
	    afterPoint2 = (value - (uint16_t)value);
	    afterPoint = afterPoint2*100;

	    do
	    {
	        buffer[cnt++] = (uint16_t)afterPoint % 10 + '0'; // Resto da divisao, sempre o primeiro digito
	        afterPoint = afterPoint / 10; //exclui o digito fazendo a divisao
	    }
	    while(afterPoint); // repete ate valor n==0

	    buffer[cnt++] = 0x2E;
	    do
		{
			buffer[cnt++] = (uint16_t)beforePoint % 10 + '0'; // Resto da divisao, sempre o primeiro digito
			beforePoint = beforePoint / 10; //exclui o digito fazendo a divisao
		}
		while(beforePoint); // repete ate valor n==0

	    if(flag_negativo)
	    {
	        buffer[++cnt] = 0x2D;//'-'
	    }

	    for(; cnt; cnt--)
	    {
	        if(buffer[cnt] == 0x2D)
	        {
	            xprint_putc(func, 0x2D);
	        }
	        else
	        {
	            xprint_putc(func, buffer[cnt - 1] ); //soma o valor do buffer com o valor do caracter zero 0x30.
	        }
	    }
}

void xprintf(void (*func)(uint8_t), const uint8_t *fmt, ...)
{
    va_list pa;
    uint8_t *s, c;
    int16_t d;
    uint16_t u;
    float f;

    va_start(pa, fmt);
    
	//while(*fmt != '\0')
	while(*fmt)
	{
		if(*fmt == '%')
		{
			switch(*++fmt)
			{
				case '%':
                    xprint_putc(func, '%');
				break;

				case 'c': /* uint8*/
					c = va_arg(pa, int);
					xprint_putc(func, (int8_t)c);
				break;

				case 's': /* string */
					s = va_arg(pa, uint8_t *);
					xprint_string(func, s);
				break;

				case 'd': /* int16*/
					d = va_arg(pa, int);
					xprint_INT16(func, (int16_t)d);
				break;

				case 'u': /* uint16*/
					u = va_arg(pa, int);
					xprint_UINT16(func, (uint16_t)u);
				break;

                case 'f': /* float*/
                    f = va_arg(pa, double);
                    xprint_float(func, f);
                break;
			}
		}
		else
		{
			xprint_putc(func, *fmt);
		}

		/*incrementa o ponteiro*/
		fmt++;
	}
    va_end(pa);
}

