/**
  * @file    leds.h
  * @author  Alexandre Bader; Jorge Guzman (jorge.gzm@gmail.com); Rafael lopes (faellf@hotmail.com); 
  * @date    Apr 23, 2014
  * @version 0.3.0.0 (beta)
  * @brief   Bibliteoca para o uso dos Leds
  * @details
  * @section LICENSE
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License as
  * published by the Free Software Foundation; either version 2 of
  * the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * General Public License for more details at
  * http://www.gnu.org/copyleft/gpl.html
*/

#ifndef LEDS2_H_
#define LEDS2_H_

//==============================================================================
// INCLUDE FILES
//==============================================================================

#include <stdint.h>

//==============================================================================
// PUBLIC DEFINITIONS
//==============================================================================

/** @brief Numero total de leds*/
#define NUM_LEDS 8

/** @brief Acoes que o led pode realizar*/
typedef enum
{
	LED_OFF,		/**< Desliga o led*/
	LED_ON,			/**< Liga o led*/
	LED_BLINK_SLOW,	/**< Faz o led piscar lentamente */
	LED_BLINK_FAST  /**< Faz o led piscar rapidamente */
}LEDS_ACTION;

//==============================================================================
// PUBLIC TYPEDEFS
//==============================================================================

/** @brief Representação de todos os led para uso nas funcaos leds_set e leds_attach.*/
typedef enum
{
	N_LED1 = 1<<0, /**< Representacao em byte do led 1 */
	N_LED2 = 1<<1, /**< Representacao em byte do led 2 */
	N_LED3 = 1<<2, /**< Representacao em byte do led 3 */
	N_LED4 = 1<<3, /**< Representacao em byte do led 4 */
	N_LED5 = 1<<4, /**< Representacao em byte do led 5 */
	N_LED6 = 1<<5, /**< Representacao em byte do led 6 */
	N_LED7 = 1<<6, /**< Representacao em byte do led 7 */
	N_LED8 = 1<<7, /**< Representacao em byte do led 8 */
}LEDS_NAME;

//==============================================================================
// PUBLIC VARIABLES			
//==============================================================================

//==============================================================================
// PUBLIC FUNCTIONS
//==============================================================================

/**
 * Limpa o vetor que armazena pinos I/O que serao usados pela lib e inicializa
 * as variaveis auxiliares.
 */
void leds_init(void);

/**
 * Funcao para o controle do acendimento dos leds.
 * @param in_leds indica os leds que serao usados.
 * @param action acao que os leds indicados deverao realizar.
 */
void leds_set(LEDS_NAME in_leds, LEDS_ACTION action);

/**
 * Verifica se o timer para a reversao dos leds estourou tanto no slow como no fast
 * esta funcao deve ser chamada a cada de 100 mili segundos.
 */
void leds_action_isr_100ms(void);

/**
 * Funcao que configura um led atribuindo a ele uma representação de pino I/O.
 * @param Led que sera configurado (Range: N_LED1 a N_LED8).
 * @param pin Pino I/O que deseja se representar.
 */
void leds_attach(LEDS_NAME index, int pin);

/**
 * Funcao usada para monitoramento do status dos leds.
 * @return retorna status dos leds
 */
uint8_t leds_status(void);

#endif 
