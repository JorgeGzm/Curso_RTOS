/**
 * @file    leds.c
 * @author  Alexandre Bader; Jorge Guzman (jorge.gzm@gmail.com); Rafael lopes (faellf@hotmail.com);
 * @date    Apr 23, 2014
 * @version 0.3.0.0 (beta)
 * @brief   Bibliteoca para o uso dos Leds
 * @details
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 */

//==============================================================================
// INCLUDE FILES
//==============================================================================

#include "leds.h"
#include "bitwise/bitwise.h"
#include "gpio/hal_gpio.h"

//==============================================================================
// PRIVATE DEFINITIONS
//==============================================================================

/** @brief intervalo de tempo x100ms de delay para gerar o blink slow*/
#define LED_BLINK_SLOW_STEP 10

/** @brief intervalo de tempo x100ms de delay para gerar o blink fast*/
#define LED_BLINK_FAST_STEP 3

//==============================================================================
// PRIVATE TYPEDEFS
//==============================================================================

//==============================================================================
// PRIVATE VARIABLES			
//==============================================================================

/**@brief Armazena os pinos I/O que serao usados. */
uint8_t leds[NUM_LEDS];

/** @brief Controle imediato do estado das leds */
uint8_t ledsStatus;

/** @brief Indicao das leds que estao 100% ligadas */
uint8_t ledsON;

/** @brief Indicao das Leds que estao picando lentamnete */
uint8_t leds_blink_slow;

/** @brief Indicao das Leds que estao piscando rapido */
uint8_t leds_blink_fast;

//==============================================================================
// PRIVATE FUNCTIONS
//==============================================================================

/**
 * Verifica quais leds serao ligados
 * @param leds_mask: mascara que contem quais leds serao ligados.
 */
static void leds_on(uint8_t leds_mask);

/**
 * Verifica quais leds serao desligados
 * @param leds_mask: mascara que contem quais leds serao desligados
 */
static void leds_off(uint8_t leds_mask);

/**
 * Inverte os leds representados pelos bits 1 da mascara de entrada
 * @param leds_mask: mascara que contem quais leds serao invertidos
 */
static void leds_reverse(uint8_t leds_mask);

/**
 * Escreve o valor de saida
 * @param out Valor de saida representado por bits
 */
static void leds_write(uint8_t out);

//==============================================================================
// SOURCE CODE
//==============================================================================

void leds_init(void)
{
	uint8_t i;

	ledsStatus= 0;
	ledsON = 0;
	leds_blink_slow = 0;
	leds_blink_fast = 0;

	for(i = 0; i < NUM_LEDS; i++)
	{
		leds[i] = 0xFF;
	}
}

void leds_set(LEDS_NAME in_leds, LEDS_ACTION action)
{
	// auxiliar para operacoes logicas
	uint8_t leds_temp;

	// verifica qual acao que as leds em questao deve executar
	switch (action)
	{
	case LED_OFF:
	{
		// XOR seguido de um AND para desligar os bits
		// Apaga as leds 100% ligadas
		leds_temp = ledsON ^ in_leds;
		ledsON = ledsON & leds_temp;

		// apaga as leds com blink lento
		leds_temp = leds_blink_slow ^ in_leds;
		leds_blink_slow = leds_blink_slow & leds_temp;

		// apaga as leds com blink rapido
		leds_temp = leds_blink_fast ^ in_leds;
		leds_blink_fast = leds_blink_fast & leds_temp;

		// apaga as leds da mascara
		leds_off(in_leds);

		break;
	}
	case LED_ON:
	{
		// devemos ascender as leds em questao
		ledsON = ledsON | in_leds;

		// desliga as leds com blink lento
		leds_temp = leds_blink_slow ^ in_leds;
		leds_blink_slow = leds_blink_slow & leds_temp;

		// desliga as leds com blink rapido
		leds_temp = leds_blink_fast ^ in_leds;
		leds_blink_fast = leds_blink_fast & leds_temp;

		// acende as leds definidas
		leds_on(ledsON);

		break;
	}
	case LED_BLINK_SLOW:
	{
		// Apaga as leds 100% ligadas
		leds_temp = ledsON ^ in_leds;
		ledsON = ledsON & leds_temp;

		// ativa as leds com blink lento
		leds_blink_slow  = leds_blink_slow  | in_leds;

		// apaga as leds com blink rapido
		leds_temp = leds_blink_fast  ^ in_leds;
		leds_blink_fast  = leds_blink_fast  & leds_temp;

		break;
	}
	case LED_BLINK_FAST:
	{
		// Apaga as leds 100% ligadas
		leds_temp = ledsON  ^ in_leds;
		ledsON  = ledsON  & leds_temp;

		// apaga as leds com blink lento
		leds_temp = leds_blink_slow  ^ in_leds;
		leds_blink_slow  = leds_blink_slow  & leds_temp;

		// ativa as leds com blink rapido
		leds_blink_fast  = leds_blink_fast  | in_leds;

		break;
	}
	}
}

void leds_attach(LEDS_NAME index, int pin)
{
	leds[countUntilFirstByteOne(index)] = pin;
	//pinMode(pin, OUTPUT);
}

void leds_action_isr_100ms(void)
{
	static uint8_t timer_led_slow_reverse_counter = 0; // variavel de controle do blink lento
	static uint8_t timer_led_fast_reverse_counter = 0; // controle do blink rapido

	// soma 1 nos contadores
	timer_led_slow_reverse_counter++;
	timer_led_fast_reverse_counter++;

	// verifica se devemos fazer a reversao para os slow blinks
	if (timer_led_slow_reverse_counter == LED_BLINK_SLOW_STEP)
	{
		// sim, o contador do slow estourou, fazemos a reversao
		leds_reverse(leds_blink_slow );

		// zera o valor do contador apos a reversao
		timer_led_slow_reverse_counter = 0;
	}

	// verifica se devemos fazer a reversao para os fast blinks
	if (timer_led_fast_reverse_counter == LED_BLINK_FAST_STEP)
	{
		// sim, o contador do slow estourou, fazemos a reversao
		leds_reverse(leds_blink_fast );

		// zera o valor do contador apos a reversao
		timer_led_fast_reverse_counter = 0;
	}
}

void leds_reverse(uint8_t leds_mask)
{
	//auxiliar para operacaes logicas
	uint8_t leds_temp;

	// verifica se a mascara tem algum bit ativo
	if (leds_mask)
	{
		// Tem, entao executa a operacao de inversao
		// copia o status para var temp
		leds_temp = ledsStatus ;

		// ativa os bits definidos na mascara
		leds_on(leds_mask);

		// verifica se o valor e igual ao anterior, isso significa que os bits ja estava ligados
		if (leds_temp == ledsStatus )
		{
			// entao devemos desligar os bits, pois eles ja estavam ativos
			leds_off(leds_mask);
		}
	}
}

void leds_on(uint8_t leds_mask)
{
	uint8_t aux;
	// executa um OU para ativar os bits
	ledsStatus = ledsStatus | leds_mask;

	//Atualiza os LEDs no frontal
	aux = leds_status();
	aux |= ledsStatus ;

	leds_write(aux);
}

void leds_off(uint8_t leds_mask)
{
	uint8_t aux;
	uint8_t leds_temp;

	// devemos desligar os bits
	leds_temp = ledsStatus  ^ leds_mask;
	ledsStatus  = ledsStatus  & leds_temp;

	aux = ledsStatus;
	aux &= ~leds_mask;

	//Atualiza os LEDs no frontal
	leds_write(aux);
}

uint8_t leds_status(void)
{
	uint8_t index;
	uint8_t status;
	status = 0;

	for(index = 0; index < NUM_LEDS; index++)
	{
		if((leds[index] != 0xFF) && digitalRead(leds[index]))
		{
			status |=  (1 << index);
		}
	}

	return(status);
}

void leds_write(uint8_t out)
{
	uint8_t index;

	for (index = 0; index < NUM_LEDS; index++)
	{
		if(leds[index] != 0xFF)
		{
			if (tst_bit(out, index))
			{
				digitalWrite(leds[index], LOW);
			}
			else
			{
				digitalWrite(leds[index], HIGH);
			}
		}
	}
}

