/**
  * @file    lcd.h
  * @author  Jorge Guzman (jorge.gzm@gmail.com); Rafael lopes (faellf@hotmail.com); 
  * @date    Jul 9, 2014
  * @version 0.2.0.0 (beta)
  * @brief   Bibliteoca para o uso do display LCD 2x16 ou 4x20.
  * @details
  * @section LICENSE
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License as
  * published by the Free Software Foundation; either version 2 of
  * the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  * General Public License for more details at
  * http://www.gnu.org/copyleft/gpl.html
*/

#ifndef LCD_H
	#define	LCD_H

//==============================================================================
// INCLUDE FILES
//==============================================================================

#include <stdint.h>
#include "gpio/hal_gpio.h"
#include "delay/hal_delay.h"

//==============================================================================
// PUBLIC DEFINITIONS
//==============================================================================


//==============================================================================
// PUBLIC TYPEDEFS
//==============================================================================

//==============================================================================
// PUBLIC VARIABLES			
//==============================================================================

//==============================================================================
// PUBLIC FUNCTIONS
//==============================================================================

/**
 * Funcao que configura uma display LCD atribuindo a ele uma representação dos pinos I/O.
 * @param RS Pino I/O que representara RS.
 * @param E Pino I/O que representara E.
 * @param DB4 Pino I/O que representara DB4.
 * @param DB5 Pino I/O que representara DB5.
 * @param DB6 Pino I/O que representara DB6.
 * @param DB7 Pino I/O que representara DB7.
 */
void lcd_attach(uint8_t RS, uint8_t E, uint8_t DB4, uint8_t DB5, uint8_t DB6, uint8_t DB7);

/**
* Posiciona o cursor na posicao (x,y) do display LCD.
* Tendo como limite superior (1,1) e o limete inferior (1,4).
* @param x: coluna
* @param y: linha
*/
int16_t lcd_gotoxy(uint8_t x, uint8_t y);

/**
 * Funcao responsavel por imprimir um caracter no display LCD.
 * @param c caracter que sera impresso.
 */
void lcd_putc(uint8_t c);

/**
 * Funcao responsavel por imprimir uma string no display LCD.
 * @param
 */
void lcd_print(const uint8_t *str);

#endif	

